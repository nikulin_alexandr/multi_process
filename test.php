<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.09.15
 * Time: 14:25
 */

include_once __DIR__.DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";


use Sasa\Thread\Example\SimpleThread;

$s1 = new SimpleThread();
$s2 = new SimpleThread();
//$s1->setConfigPath(__DIR__.DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."thread.php");
$s1->setParams(["a"=>1]);
//$s2->setConfigPath(__DIR__.DIRECTORY_SEPARATOR."config".DIRECTORY_SEPARATOR."thread.php");
$s1->start();
$s2->start();


while(!$s1->isReady()){
    sleep(1);
}
while(!$s2->isReady()){
    sleep(1);
}

echo $s1->getResult()." ".$s2->getResult();
