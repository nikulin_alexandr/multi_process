<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.09.15
 * Time: 14:20
 */

include_once __DIR__.DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php";

use Sasa\Thread\AbstractThread;

echo AbstractThread::startAction($_SERVER['argv']);