<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.09.15
 * Time: 14:20
 */
$autoloadFiles = array(__DIR__ . '/../vendor/autoload.php',
                       __DIR__ . '/../../../autoload.php');

foreach ($autoloadFiles as $autoloadFile) {
    if (file_exists($autoloadFile)) {
        require_once $autoloadFile;
    }
}
use Sasa\Thread\AbstractThread;

echo AbstractThread::startAction($_SERVER['argv']);