<?php

namespace Sasa\Thread;

/**
 * Запуск несокльких потоков
 *
 * @author alex
 */
abstract class ContainerThread extends AbstractThread
{

    const DEFAULT_COUNT_THREADS = 5;
    
    /**
     *
     * @var mixed 
     */
    private $threadsList;


    /**
     *
     * @var mixed 
     */
    private $results;

    /**
     * генерация потоков
     */
    protected function startThreads()
    {
        
        for ($i = 0; $i < $this->getCountThreads(); $i++) {
            $this->threadsList[$i] = $this->createThread();
            $this->threadsList[$i]->start();
        }
    }

    /**
     * проверяем все ли процессы завершены
     * @return boolean
     */
    public function isFinish()
    {
        return empty($this->threadsList);
    }

    public function getCountThreads()
    {
        if(empty($this->params['countThreads'])) {
            return self::DEFAULT_COUNT_THREADS;
        }
        return $this->params['countThreads'];
    }

    

    /**
     * Обработка результата запуска потоков
     */
    protected function run()
    {
        $this->startThreads();
        $this->runElement();
        while(!$this->isFinish()) {
            $this->runElement();
        }
        
    }

    private function runElement()
    {
        $this->beforeLoadResults();
        $this->loadResults();
        $this->afterLoadResults();
    }

    /**
     * Чтение результатов из потока
     */
    private function loadResults()
    {
        foreach ($this->threadsList as $key => $item) {
            if ($item->isReady()) {
                $this->results[] = $item->getResult();
                unset($this->threadsList[$key]);
            }
        }
    }

    /**
     * получаем результаты
     * @return type
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * Генерация потока
     * @return AbstractThread
     */
    abstract protected function createThread();

    /**
     * Функция до чтения результатов из потоков
     */
    abstract protected function beforeLoadResults();

    /**
     * функция после чтения результатов из потока
     */
    abstract protected function afterLoadResults();
}
