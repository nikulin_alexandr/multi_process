<?php

namespace Sasa\Thread;

/**
 * Абстратный класс для вызова многопоточности
 *
 * @author alex
 */
abstract class AbstractThread implements IThread
{

    /**
     * Путь к команде php
     * @var String
     */
    protected $pathPhp = null;
    private $waitTime = 0;

    /**
     * Каналы передачи данных
     * @var mixed
     */
    private $pipes;

    /**
     * Текущий процесс
     * @var \Resource
     */
    private $process;

    /**
     * передоваемые параметры
     * @var mixed
     */
    protected $params;

    /**
     * Конифгурация подключения
     * @var
     */
    protected $configPath;

    public function __construct($phpPath="php")
    {
        $this->pathPhp = $phpPath;
    }

    /**
     * запуск потока
     */
    public function start()
    {

        $command = $this->createCommand();
        $descriptorSpec = [
            1 => ['pipe', 'w']
        ];
        $this->process = proc_open($command, $descriptorSpec, $this->pipes);
    }

    /**
     * создает выполнение команды
     *
     * @return type
     * @throws \Exception
     */
    public function createCommand()
    {

        if (empty($this->pathPhp)) {
            throw new \Exception("Путь к php команды не инициализирован" .
                "для unix платформ это php, для windows c:\\php\php.exe");
        }
        $addParams = "";
        if(is_array($this->params)) {
            $addParams = implode(" ", $this->params);
        }
        $className = str_replace("\\", "\\\\", get_class($this));

        return $this->pathPhp . ' -f run.php ' . $className . ' "' . $this->configPath . '" '.$addParams;
    }

    /**
     * Расчет закончен
     *
     * @return bool
     * @throws ExceptionThread
     */
    public function isReady()
    {

        $read = [$this->pipes[1]];
        if($this->pipes[1]==NULL){
            proc_close($this->process);
            return true;
        }
        $res = stream_select($read, $write = null, $except = null, $this->waitTime);


        if ($res === FALSE) {
            proc_close($this->process);
            throw new ExceptionThread("Поток завершился фатальной ошибкой");
        }
        if ($res < 0) {
            proc_close($this->process);
            throw new ExceptionThread("Истек таймлимит");
        }

        return $res > 0;
    }

    /**
     * добавить параметр
     * @param type $key
     * @param type $value
     */
    public function addParams($key, $value)
    {
        $this->params[$key] = $value;
    }

    /**
     * Получаем результат. если результата пока нет получаем NULL
     * @return String
     */
    public function getResult()
    {
        if($this->pipes[1]==null){
            return NULL;
        }
        if ($this->isReady()) {
            $result = stream_get_contents($this->pipes[1]);
            return $result;
        }

        return NULL;
    }

    /**
     *
     * @param type $params
     * @return AbstractThread
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }


    /**
     *
     * @return type
     * @throws \Exception
     */
    public static function loadArgs($args)
    {

        if (!isset($args[1])) {
            throw new \Exception("Не передано имя класса для потока");
        }

        $config = ['php-path'=>"php"];
        if(!empty($args[2])) {
            if (!file_exists($args[2])) {
                throw new \Exception("Файл не найден");
            }
            $config = require $args[2];
        }

        $className = new $args[1]($config['php-path']);
        if (count($args)>3) {
            $_a = [];
            for($i=3;$i<count($args);$i++){
                $_a[$i-3] = $args[$i];
            }
            $className->setParams($_a);
        }

        return $className;
    }

    public static function validateCommand($command)
    {
        $comment = "(php-path находится в массиве конфигураций в файле config/thread.php)";
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            if (!file_exists($command)) {
                throw new \Exception("php-path ссылка на php.exe " . $comment);
            }
        } else {
            if ($command != "php") {
                throw new \Exception("php-path должен быть равным php " . $comment);
            }
        }
    }

    public static function startAction($args)
    {
        $class = self::loadArgs($args);
        if(!($class instanceof IThread)) {
            throw new Exception("Вызываемый класс в потоке должен "
                . "быть унаследован от IThread");
        }
        return $class->run();
    }

    /**
     * @return mixed
     */
    public function getConfigPath()
    {
        return $this->configPath;
    }

    /**
     * @param mixed $configPath
     */
    public function setConfigPath($configPath)
    {
        $this->configPath = $configPath;
    }



    abstract protected function run();
}
