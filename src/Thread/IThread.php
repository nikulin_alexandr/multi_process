<?php

namespace Sasa\Thread;

/**
 *
 * @author alex
 */
interface IThread
{

    public function start();
}
