<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 23.09.15
 * Time: 14:23
 */

namespace Sasa\Thread\Example;

use Sasa\Thread\AbstractThread;

class SimpleThread extends AbstractThread {

    protected function run()
    {
        $r = 1;
        for($i=1;$i<20;$i++){

            $r*=$i;
        }

        return $r.print_r($this->params,true);
    }
}